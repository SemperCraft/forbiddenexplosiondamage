package net.sempercraft.mineyannik.forbiddenexplosiondamage;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class ForbiddenExplosionDamage extends JavaPlugin implements Listener {

    public void onEnable()
    {
        this.getServer().getPluginManager().registerEvents(this, this);
    }

    @EventHandler
    public void onExplosion(EntityExplodeEvent e)
    {
        //Cancel default Explosion
        e.setCancelled(true);

        //Create a new Explosion on the same location, with the same force but without block breaking and fire
        World world = e.getEntity().getWorld();
        Location loc = e.getEntity().getLocation();
        world.createExplosion(loc.getX(), loc.getY(), loc.getZ(), e.getYield(), false, false);
    }

}
